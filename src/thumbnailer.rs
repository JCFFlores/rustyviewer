use crate::message;
use crate::utils;
use crossbeam_channel;
use crossbeam_channel::{Receiver, Sender};
use image::imageops;
use image::GenericImageView;
use log::{debug, error, trace};
use std::path::PathBuf;
use std::sync::Arc;
use std::thread;
use std::vec::Vec;

pub enum ThumbnailerSignals {
	NoOp,
	Stop,
}

pub type ThumbnailerSignalerSender = Sender<ThumbnailerSignals>;
pub type ThumbnailerSignalerReceiver = Receiver<ThumbnailerSignals>;

pub fn create_thumbnailer(
	image_list: Arc<Vec<PathBuf>>,
	thumbnail_rect: utils::Rectangle,
	transmitter: message::RustyMessageTransmitter,
) -> ThumbnailerSignalerSender {
	let (tx, rx): (ThumbnailerSignalerSender, ThumbnailerSignalerReceiver) =
		crossbeam_channel::unbounded();
	thread::spawn(move || {
		for path in image_list.iter() {
			let signal = rx.try_recv().unwrap_or(ThumbnailerSignals::NoOp);
			if let ThumbnailerSignals::Stop = signal {
				debug!("Stopping thumbnailer");
				break;
			}
			trace!("Thumbnailing image with path: {}", path.display());
			let base_image = match image::open(path) {
				Ok(img) => img,
				Err(e) => {
					error!("Failed to open image with error: {}", e);
					transmitter
						.send(message::RustyMessage::ExitMessage)
						.unwrap_or(());
					break;
				}
			};
			let image_rectangle = utils::Rectangle {
				width: base_image.width() as i16,
				height: base_image.height() as i16,
			};
			let resize_dimensions = utils::resize_dimensions(&thumbnail_rect, &image_rectangle);
			let resized_image = base_image
				.resize_exact(
					resize_dimensions.width as u32,
					resize_dimensions.height as u32,
					imageops::FilterType::CatmullRom,
				)
				.to_bgra8();
			match transmitter.send(message::RustyMessage::Thumbnail(
				resized_image,
				path.clone(),
			)) {
				Err(e) => {
					error!("Failed to send message to main loop with error: {}", e);
					break;
				}
				_ => (),
			}
		}
	});
	tx
}
