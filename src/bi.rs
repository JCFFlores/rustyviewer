pub struct BidirectionalRun<'a, T> {
	collection: &'a [T],
	index: usize,
}

impl<'a, T> BidirectionalRun<'a, T> {
	pub fn get_item(&self) -> Option<&'a T> {
		self.collection.get(self.index)
	}

	pub fn next(&mut self) -> Option<&'a T> {
		if self.index < self.collection.len() - 1 {
			self.index = self.index + 1;
			self.get_item()
		} else {
			None
		}
	}

	pub fn prev(&mut self) -> Option<&'a T> {
		if self.index > 0 {
			self.index = self.index - 1;
			self.get_item()
		} else {
			None
		}
	}
}

impl<'a, T: Eq> BidirectionalRun<'a, T> {
	pub fn new(collection: &'a [T], elem: &T) -> Option<BidirectionalRun<'a, T>> {
		let index = collection.iter().position(|candidate| candidate == elem);
		index.map(move |i| BidirectionalRun {
			index: i,
			collection,
		})
	}
}
