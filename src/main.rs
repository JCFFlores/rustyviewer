use log::{debug, error, info, trace, warn};
use rustyviewer::constants;
use rustyviewer::message::RustyMessage;
use rustyviewer::message::{RustyMessageReceiver, RustyMessageTransmitter};
use rustyviewer::setup;
use rustyviewer::single_viewer::SingleImageViewer;
use rustyviewer::single_worker;
use rustyviewer::thumbnail_viewer::ThumbnailViewer;
use rustyviewer::utils;
use rustyviewer::viewer::ImageViewer;
use rustyviewer::window::Window;
use std::error::Error;
use std::sync::Arc;
use std::thread;
use structopt::StructOpt;

fn main() -> Result<(), Box<dyn Error>> {
	env_logger::init();
	let args = rustyviewer::cli::Cli::from_args();

	let image_list = {
		if args.thumbnail {
			utils::thumbnail_image_list(&args.path).map(Arc::new)?
		} else {
			match utils::get_image_list(&args.path) {
				Ok(list) => Arc::new(list),
				Err(e) => {
					error!("Failed to get image list with error: {}", e);
					return Err(e);
				}
			}
		}
	};

	let (connection, screen_number) = match xcb::Connection::connect(None) {
		Ok((connection, screen_number)) => (Arc::new(connection), screen_number),
		Err(e) => {
			error!("Failed to get a connection to X server with error: {}", e);
			return Err(Box::new(e));
		}
	};

	let screen = connection
		.get_setup()
		.roots()
		.nth(screen_number as usize)
		.unwrap();

	debug!(
		"Screen resolution: {}x{}",
		screen.width_in_pixels(),
		screen.height_in_pixels()
	);

	debug!("Setting window size to {}x{}", args.width, args.height);

	let window = Window::new(
		&connection,
		&screen,
		args.width,
		args.height,
		args.border_width,
		&[
			(
				xcb::CW_EVENT_MASK,
				xcb::EVENT_MASK_EXPOSURE
					| xcb::EVENT_MASK_KEY_PRESS
					| xcb::EVENT_MASK_STRUCTURE_NOTIFY,
			),
			(xcb::CW_BACK_PIXEL, screen.black_pixel()),
		],
	);

	let close_atom = setup::notify_window_close(&connection, &window);

	let (transmitter, receiver): (RustyMessageTransmitter, RustyMessageReceiver) =
		crossbeam_channel::unbounded();

	let connection_for_thread = connection.clone();
	let transmitter_for_thread = transmitter.clone();
	let transmitter_for_worker = transmitter.clone();

	thread::spawn(move || loop {
		let rust_message = connection_for_thread
			.wait_for_event()
			.map_or(RustyMessage::ExitMessage, RustyMessage::XEvent);

		match transmitter_for_thread.send(rust_message) {
			Ok(_) => (),
			Err(_) => break,
		}
	});

	let mut context = utils::Context {
		connection: &connection,
		window,
		screen: &screen,
	};

	let single_job_transmitter = single_worker::create_single_job_worker(transmitter_for_worker);

	let mut viewer: Box<dyn ImageViewer> = {
		if args.thumbnail {
			Box::new(ThumbnailViewer::new(
				&context,
				Arc::clone(&image_list),
				transmitter.clone(),
				single_job_transmitter.clone(),
				0,
			))
		} else {
			info!("Opening file: {}", args.path.display());

			let initial_image = match image::open(&args.path).map(|img| img.to_bgra8()) {
				Ok(img) => Arc::new(img),
				Err(e) => {
					error!("Failed to open image with error: {}", e);
					return Err(Box::new(e));
				}
			};
			SingleImageViewer::new(
				initial_image,
				args.path,
				&image_list[..],
				transmitter.clone(),
				single_job_transmitter.clone(),
				&context,
			)
			.map(Box::new)
			.unwrap()
		}
	};
	context.window.map();
	connection.flush();

	loop {
		if let Err(e) = connection.has_error() {
			error!("Error in connection to X server: {}", e);
			break;
		}

		let message = match receiver.recv() {
			Ok(message) => message,
			Err(e) => {
				error!("Failed to get message with error: {}", e);
				return Err(Box::new(e));
			}
		};

		match message {
			RustyMessage::XEvent(event) => {
				let response_type = event.response_type() & constants::RESPONSE_TYPE_MASK;
				match response_type {
					xcb::EXPOSE => {
						viewer.draw(&context);
					}
					xcb::KEY_PRESS => {
						let key_press: &xcb::KeyPressEvent = unsafe { xcb::cast_event(&event) };
						trace!("Pressed key: {}", key_press.detail());
						match key_press.detail() {
							constants::ESC_KEY | constants::Q_KEY => break,
							_ => viewer.handle_key_press(key_press, &context),
						}
					}
					xcb::CONFIGURE_NOTIFY => {
						let configure_notify: &xcb::ConfigureNotifyEvent =
							unsafe { xcb::cast_event(&event) };
						if configure_notify.window() == context.window.xcb_window() {
							debug!(
								"Resizing window from {}x{} to {}x{}",
								context.window.width(),
								context.window.height(),
								configure_notify.width(),
								configure_notify.height()
							);
							context.window.set_width(configure_notify.width());
							context.window.set_height(configure_notify.height());
							viewer.handle_resize(&context);
						}
					}
					xcb::CLIENT_MESSAGE => {
						if let Some(atom) = close_atom {
							let client_message: &xcb::ClientMessageEvent =
								unsafe { xcb::cast_event(&event) };
							if client_message.data().data32().contains(&atom) {
								break;
							}
						}
					}
					_ => (),
				}
			}
			RustyMessage::ImageOpen(img, path) => {
				viewer = Box::new(SingleImageViewer::new(
					Arc::new(img),
					path,
					&image_list[..],
					transmitter.clone(),
					single_job_transmitter.clone(),
					&context,
				))
				.map(Box::new)
				.unwrap();
				context.window.clear();
				viewer.draw(&context);
			}
			RustyMessage::ImageResize(img) => {
				viewer.handle_resized_image(img, &context);
			}
			RustyMessage::Thumbnail(img, path) => {
				viewer.handle_thumbnail(img, path, &context);
			}
			RustyMessage::StartThumbnailViewer(path) => {
				let index = match image_list.iter().position(|candidate| candidate == &path) {
					Some(index) => index,
					None => {
						warn!("Unable to establish selector position, setting to 0");
						0
					}
				};
				context.window.clear();
				viewer = Box::new(ThumbnailViewer::new(
					&context,
					Arc::clone(&image_list),
					transmitter.clone(),
					single_job_transmitter.clone(),
					index,
				));
			}
			RustyMessage::ExitMessage => break,
		}
	}
	Ok(())
}
