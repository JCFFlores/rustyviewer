use crate::constants;
use crate::message::RustyMessageTransmitter;
use crate::single_worker::{RustySingleJob, RustySingleJobTransmitter};
use crate::thumbnail::Thumbnail;
use crate::thumbnailer;
use crate::utils;
use crate::viewer::ImageViewer;
use crate::window::Window;
use log::{debug, trace, warn};
use std::collections::BTreeMap;
use std::ops::Drop;
use std::path::PathBuf;
use std::sync::Arc;
use std::vec::Vec;

pub struct ThumbnailViewer<'a> {
	rectangle_context: xcb::Gcontext,
	connection: &'a xcb::Connection,
	rows: u16,
	cols: u16,
	index: usize,
	images_len: usize,
	top_row: usize,
	thumbnails: BTreeMap<PathBuf, Thumbnail<'a>>,
	single_job_transmitter: RustySingleJobTransmitter,
	thumbnailer_signaler: thumbnailer::ThumbnailerSignalerSender,
}

fn no_zero(n: u16) -> u16 {
	if n == 0 {
		1
	} else {
		n
	}
}

fn grid_size(window: &Window) -> (u16, u16) {
	let full_thumbnail_width = constants::THUMBNAIL_WIDTH + constants::THUMBNAIL_PADDING;
	let full_thumbnail_height = constants::THUMBNAIL_HEIGHT + constants::THUMBNAIL_PADDING;
	let rows = window.height() / full_thumbnail_width as u16;
	let cols = window.width() / full_thumbnail_height as u16;
	(no_zero(rows), no_zero(cols))
}

impl<'a> ThumbnailViewer<'a> {
	pub fn new(
		context: &utils::Context<'a>,
		image_list: Arc<Vec<PathBuf>>,
		transmitter: RustyMessageTransmitter,
		single_job_transmitter: RustySingleJobTransmitter,
		index: usize,
	) -> ThumbnailViewer<'a> {
		let thumbnail_rect = utils::Rectangle {
			width: constants::THUMBNAIL_WIDTH,
			height: constants::THUMBNAIL_HEIGHT,
		};
		let images_len = image_list.len();
		let thumbnailer_signaler =
			thumbnailer::create_thumbnailer(image_list, thumbnail_rect, transmitter);
		let rectangle_context = context.connection.generate_id();
		xcb::create_gc(
			context.connection,
			rectangle_context,
			context.window.xcb_window(),
			&[
				(xcb::GC_FOREGROUND, context.screen.white_pixel()),
				(xcb::GC_GRAPHICS_EXPOSURES, 0),
				(xcb::GC_LINE_WIDTH, constants::RECTANGLE_LINE_WIDTH),
			],
		);
		let (rows, cols) = grid_size(&context.window);
		let thumbnails = BTreeMap::new();
		let top_row = index / cols as usize;
		debug!("rows: {} cols: {} top_row: {}", rows, cols, top_row);
		ThumbnailViewer {
			connection: context.connection,
			rectangle_context,
			rows,
			cols,
			thumbnails,
			index,
			images_len,
			top_row,
			single_job_transmitter,
			thumbnailer_signaler,
		}
	}

	fn adjust_top_row(&mut self) {
		let candidate_row = self.index / self.cols as usize;
		if candidate_row < self.top_row {
			self.top_row = self.top_row - 1;
		} else if candidate_row >= self.top_row + self.rows as usize {
			self.top_row = self.top_row + 1;
		}
		trace!("top_row: {}", self.top_row);
	}
}

impl<'a> Drop for ThumbnailViewer<'a> {
	fn drop(&mut self) {
		xcb::free_gc(self.connection, self.rectangle_context);
		match self
			.thumbnailer_signaler
			.send(thumbnailer::ThumbnailerSignals::Stop)
		{
			Ok(_) => debug!("Sent message to stop thumbnailer"),
			Err(e) => warn!(
				"Failed to send message to stop thumbnailer with error: {}",
				e
			),
		}
	}
}

impl<'a> ImageViewer<'a> for ThumbnailViewer<'a> {
	fn handle_key_press(&mut self, event: &xcb::KeyPressEvent, context: &utils::Context) {
		match event.detail() {
			constants::H_KEY => {
				if self.index > 0 {
					self.index = self.index - 1;
					self.adjust_top_row();
					context.window.clear();
					self.draw(context);
				}
			}
			constants::L_KEY => {
				if self.index < self.images_len - 1 {
					self.index = self.index + 1;
					self.adjust_top_row();
					context.window.clear();
					self.draw(context);
				}
			}
			constants::K_KEY => {
				if self.index >= self.cols as usize {
					self.index = self.index - self.cols as usize;
					self.adjust_top_row();
					context.window.clear();
					self.draw(context);
				}
			}
			constants::J_KEY => {
				if (self.index + self.cols as usize) < self.images_len {
					self.index = self.index + self.cols as usize;
					self.adjust_top_row();
					context.window.clear();
					self.draw(context);
				}
			}
			constants::ENTER_KEY => {
				let image_path = match self.thumbnails.keys().nth(self.index) {
					Some(path) => path,
					None => {
						warn!("Unable to get path for selected image");
						return ();
					}
				};
				let message = RustySingleJob::OpenRustyImage(image_path.to_path_buf());
				self.single_job_transmitter.try_send(message).unwrap_or(())
			}
			_ => (),
		}
		trace!("Index value: {}", self.index);
	}

	fn draw(&self, context: &utils::Context<'a>) {
		let thumbnail_rectangle = utils::Rectangle {
			width: constants::THUMBNAIL_WIDTH + constants::THUMBNAIL_PADDING,
			height: constants::THUMBNAIL_HEIGHT + constants::THUMBNAIL_PADDING,
		};
		let thumbnail_area_rectangle = utils::Rectangle {
			width: self.cols as i16 * thumbnail_rectangle.width,
			height: self.rows as i16 * thumbnail_rectangle.height,
		};
		let utils::Coordinates { x, y } =
			utils::center_in_rectangle(&context.window.as_rectangle(), &thumbnail_area_rectangle);
		let start = self.top_row * self.cols as usize;
		let end = start + (self.rows as usize * self.cols as usize);
		for (i, thumbnail) in self.thumbnails.values().enumerate() {
			if i >= start && i < end {
				let thumbnail_coordinates = utils::Coordinates {
					x: x + thumbnail_rectangle.width * (i % self.cols as usize) as i16,
					y: y + thumbnail_rectangle.height
						* ((i / self.cols as usize) - self.top_row) as i16,
				};
				thumbnail.draw(
					&context.window,
					&thumbnail_rectangle,
					&thumbnail_coordinates,
					self.rectangle_context,
					i == self.index,
				);
			} else if i >= end {
				break;
			}
		}
		context.connection.flush();
	}

	fn handle_resize(&mut self, context: &utils::Context) {
		let (rows, cols) = grid_size(&context.window);
		self.rows = rows;
		self.cols = cols;
		self.top_row = self.index / self.cols as usize;
		debug!(
			"rows: {} cols: {} top_row: {}",
			self.rows, self.cols, self.top_row
		);
		context.window.clear();
		self.draw(context);
	}

	fn handle_resized_image(
		&mut self,
		_image: utils::RustyImageType,
		_context: &utils::Context<'a>,
	) {
	}

	fn handle_thumbnail(
		&mut self,
		image: utils::RustyImageType,
		path: PathBuf,
		context: &utils::Context<'a>,
	) {
		let thumbnail = Thumbnail::new(
			&image,
			context.connection,
			context.screen.root_depth(),
			&context.window,
		);
		self.thumbnails.insert(path, thumbnail);
		self.draw(context);
	}
}
