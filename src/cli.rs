use std::path::PathBuf;
use structopt::StructOpt;

#[derive(StructOpt)]
#[structopt(name = "Rusty Viewer", about = "The rusty image viewer.")]
pub struct Cli {
	#[structopt(parse(from_os_str))]
	pub path: PathBuf,

	#[structopt(short, long, default_value = "10")]
	pub border_width: u16,

	#[structopt(short, long)]
	pub thumbnail: bool,

	#[structopt(short, long, default_value = "1000")]
	pub width: u16,

	#[structopt(short, long, default_value = "1000")]
	pub height: u16,
}
