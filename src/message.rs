use crate::utils;
use std::path::PathBuf;

pub enum RustyMessage {
	XEvent(xcb::GenericEvent),
	ExitMessage,
	ImageOpen(utils::RustyImageType, PathBuf),
	ImageResize(utils::RustyImageType),
	Thumbnail(utils::RustyImageType, PathBuf),
	StartThumbnailViewer(PathBuf),
}

pub type RustyMessageReceiver = crossbeam_channel::Receiver<RustyMessage>;
pub type RustyMessageTransmitter = crossbeam_channel::Sender<RustyMessage>;
