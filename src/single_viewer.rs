use crate::bi::BidirectionalRun;
use crate::constants;
use crate::message::{RustyMessage, RustyMessageTransmitter};
use crate::setup;
use crate::single_image::SingleImage;
use crate::single_worker;
use crate::single_worker::{RustySingleJob, RustySingleJobTransmitter};
use crate::utils;
use crate::viewer::ImageViewer;
use crate::window::Window;
use image::imageops;
use log::{error, warn};
use std::ffi::CString;
use std::path::PathBuf;
use std::sync::Arc;

pub struct SingleImageViewer<'a> {
	base_image: Arc<utils::RustyImageType>,
	single_image: SingleImage<'a>,
	transmitter: RustySingleJobTransmitter,
	rusty_transmitter: RustyMessageTransmitter,
	bidirectional: BidirectionalRun<'a, PathBuf>,
	image_path: PathBuf,
}

fn create_single_image<'a>(
	base_image: &utils::RustyImageType,
	context: &utils::Context<'a>,
) -> SingleImage<'a> {
	if base_image.width() as u16 > context.screen.width_in_pixels()
		|| base_image.height() as u16 > context.screen.height_in_pixels()
	{
		warn!("Attempted to display image bigger than screen resolution, forcing quick resize to match window");
		let image_rect = utils::Rectangle {
			width: base_image.width() as i16,
			height: base_image.height() as i16,
		};
		let dimensions = utils::resize_dimensions(&context.window.as_rectangle(), &image_rect);
		let resized = imageops::resize(
			base_image,
			dimensions.width as u32,
			dimensions.height as u32,
			imageops::FilterType::Nearest,
		);
		SingleImage::new(
			&resized,
			context.connection,
			context.screen.root_depth(),
			&context.window,
		)
	} else {
		SingleImage::new(
			base_image,
			context.connection,
			context.screen.root_depth(),
			&context.window,
		)
	}
}

impl<'a> SingleImageViewer<'a> {
	pub fn new(
		base_image: Arc<utils::RustyImageType>,
		path: PathBuf,
		image_list: &'a [PathBuf],
		rusty_transmitter: RustyMessageTransmitter,
		single_job_transmitter: RustySingleJobTransmitter,
		context: &utils::Context<'a>,
	) -> Option<SingleImageViewer<'a>> {
		let bidirectional = BidirectionalRun::new(image_list, &path.to_path_buf())?;
		let file_name = path.file_name().unwrap();
		let file_name_string = file_name.to_str().unwrap();
		let file_name_c_string = CString::new(file_name_string).unwrap();
		setup::change_window_name(context.connection, &context.window, &file_name_c_string);
		let single_image = create_single_image(base_image.as_ref(), context);
		let viewer = SingleImageViewer {
			base_image,
			single_image,
			transmitter: single_job_transmitter,
			bidirectional,
			image_path: path,
			rusty_transmitter,
		};
		viewer.order_resize_if_needed(&context.window);
		Some(viewer)
	}

	fn order_resize_if_needed(&self, window: &Window) {
		let image_rectangle = self.as_rectangle();
		let dimensions = utils::resize_dimensions(&window.as_rectangle(), &image_rectangle);
		if image_rectangle != dimensions {
			self.transmitter
				.try_send(RustySingleJob::ResizeImage(
					self.base_image.clone(),
					dimensions,
				))
				.unwrap_or(());
		}
	}

	fn base_image_is_enough(&self, window: &Window) -> bool {
		let image_rectangle = self.as_rectangle();
		let dimensions = utils::resize_dimensions(&window.as_rectangle(), &image_rectangle);
		image_rectangle == dimensions
	}

	fn change_to_base_image_if_necessary(&mut self, context: &utils::Context<'a>) {
		if self.base_image_is_enough(&context.window)
			&& self.as_rectangle() != self.single_image.as_rectangle()
		{
			self.single_image = SingleImage::new(
				&self.base_image,
				context.connection,
				context.screen.root_depth(),
				&context.window,
			);
		}
	}

	fn as_rectangle(&self) -> utils::Rectangle {
		utils::Rectangle {
			width: self.base_image.width() as i16,
			height: self.base_image.height() as i16,
		}
	}

	fn send_open_next_request(&mut self) {
		match self.bidirectional.next() {
			Some(path) => self
				.transmitter
				.try_send(single_worker::RustySingleJob::OpenRustyImage(path.clone()))
				.unwrap_or(()),
			None => (),
		}
	}

	fn send_open_prev_request(&mut self) {
		match self.bidirectional.prev() {
			Some(path) => self
				.transmitter
				.try_send(single_worker::RustySingleJob::OpenRustyImage(path.clone()))
				.unwrap_or(()),
			None => (),
		}
	}
}

impl<'a> ImageViewer<'a> for SingleImageViewer<'a> {
	fn handle_key_press(&mut self, event: &xcb::KeyPressEvent, _context: &utils::Context) {
		match event.detail() {
			constants::N_KEY => self.send_open_next_request(),
			constants::P_KEY => self.send_open_prev_request(),
			constants::ENTER_KEY => {
				let message = RustyMessage::StartThumbnailViewer(self.image_path.clone());
				match self.rusty_transmitter.send(message) {
					Ok(_) => (),
					Err(e) => error!(
						"Failed to send message to start thumbnail viewer with error: {}",
						e
					),
				}
			}
			_ => (),
		}
	}

	fn draw(&self, context: &utils::Context) {
		self.single_image.draw_image(&context.window);
	}

	fn handle_resize(&mut self, context: &utils::Context<'a>) {
		self.order_resize_if_needed(&context.window);
		self.change_to_base_image_if_necessary(&context);
	}

	fn handle_resized_image(&mut self, image: utils::RustyImageType, context: &utils::Context<'a>) {
		if self.base_image_is_enough(&context.window) {
			self.change_to_base_image_if_necessary(context)
		} else {
			let single_image = SingleImage::new(
				&image,
				context.connection,
				context.screen.root_depth(),
				&context.window,
			);
			self.single_image = single_image;
		}
		context.window.clear();
		self.draw(context);
	}

	fn handle_thumbnail(
		&mut self,
		_image: utils::RustyImageType,
		_path: PathBuf,
		_context: &utils::Context<'a>,
	) {
	}
}
