use std::ops::Drop;

use crate::utils;

pub struct Window<'a> {
	connection: &'a xcb::Connection,
	window: xcb::Window,
	width: u16,
	height: u16,
}

impl<'a> Window<'a> {
	pub fn new(
		connection: &'a xcb::Connection,
		screen: &xcb::Screen,
		width: u16,
		height: u16,
		border_width: u16,
		value_list: &[(u32, u32)],
	) -> Window<'a> {
		let window = connection.generate_id();
		xcb::create_window(
			&connection,
			xcb::COPY_FROM_PARENT as u8,
			window,
			screen.root(),
			0,
			0,
			width,
			height,
			border_width,
			xcb::WINDOW_CLASS_INPUT_OUTPUT as u16,
			screen.root_visual(),
			value_list,
		);
		Window {
			connection,
			window,
			width,
			height,
		}
	}

	pub fn xcb_window(&self) -> xcb::Window {
		self.window
	}

	pub fn map(&self) {
		xcb::map_window(self.connection, self.window);
	}

	pub fn width(&self) -> u16 {
		self.width
	}

	pub fn height(&self) -> u16 {
		self.height
	}

	pub fn set_width(&mut self, width: u16) {
		self.width = width;
	}

	pub fn set_height(&mut self, height: u16) {
		self.height = height;
	}

	pub fn clear(&self) {
		xcb::clear_area(
			self.connection,
			true,
			self.window,
			0,
			0,
			self.width,
			self.height,
		);
	}

	pub fn as_rectangle(&self) -> utils::Rectangle {
		utils::Rectangle {
			width: self.width as i16,
			height: self.height as i16,
		}
	}
}

impl<'a> Drop for Window<'a> {
	fn drop(&mut self) {
		xcb::destroy_window(self.connection, self.window);
		self.connection.flush();
	}
}
