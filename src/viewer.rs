use crate::utils;
use std::path::PathBuf;

pub trait ImageViewer<'a> {
	fn handle_key_press(&mut self, event: &xcb::KeyPressEvent, context: &utils::Context<'a>);

	fn draw(&self, context: &utils::Context<'a>);

	fn handle_resize(&mut self, context: &utils::Context<'a>);

	fn handle_resized_image(&mut self, image: utils::RustyImageType, context: &utils::Context<'a>);

	fn handle_thumbnail(
		&mut self,
		image: utils::RustyImageType,
		path: PathBuf,
		context: &utils::Context<'a>,
	);
}
