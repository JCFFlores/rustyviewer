use crate::message::{RustyMessage, RustyMessageTransmitter};
use crate::utils;
use crossbeam_channel::{Receiver, Sender};
use image::imageops;
use log::{debug, error, info};
use std::path::PathBuf;
use std::sync::Arc;
use std::thread;

pub enum RustySingleJob {
	OpenRustyImage(PathBuf),
	ResizeImage(Arc<utils::RustyImageType>, utils::Rectangle),
}

pub type RustySingleJobReceiver = Receiver<RustySingleJob>;
pub type RustySingleJobTransmitter = Sender<RustySingleJob>;

fn execute_job(job: RustySingleJob) -> RustyMessage {
	match job {
		RustySingleJob::OpenRustyImage(path) => {
			info!("Opening: {}", path.display());
			match image::open(&path) {
				Ok(img) => RustyMessage::ImageOpen(img.to_bgra8(), path),
				Err(e) => {
					error!("Failed to open image with error: {}", e);
					RustyMessage::ExitMessage
				}
			}
		}
		RustySingleJob::ResizeImage(img, rect) => {
			debug!(
				"Resizing image from: {}x{} to {}x{}",
				img.width(),
				img.height(),
				rect.width,
				rect.height
			);
			RustyMessage::ImageResize(imageops::resize(
				img.as_ref(),
				rect.width as u32,
				rect.height as u32,
				imageops::FilterType::Lanczos3,
			))
		}
	}
}

pub fn create_single_job_worker(transmitter: RustyMessageTransmitter) -> RustySingleJobTransmitter {
	let (tx, rx) = crossbeam_channel::bounded(1);
	thread::spawn(move || loop {
		let message = match rx.recv() {
			Ok(m) => m,
			Err(_) => break,
		};

		let result_message = execute_job(message);

		match transmitter.send(result_message) {
			Ok(_) => (),
			Err(_) => break,
		};
	});
	tx
}
