use crate::window::Window;
use image::{Bgra, ImageBuffer};
use log::{error, info};
use std::cmp::{Eq, PartialEq};
use std::collections::HashSet;
use std::error::Error;
use std::ffi::OsStr;
use std::fmt::Debug;
use std::fs;
use std::path::{Path, PathBuf};
use std::vec::Vec;

pub type RustyImageType = ImageBuffer<Bgra<u8>, Vec<u8>>;

#[derive(Clone, PartialEq, Eq, Debug)]
pub struct Rectangle {
	pub width: i16,
	pub height: i16,
}

#[derive(Clone)]
pub struct Coordinates {
	pub x: i16,
	pub y: i16,
}

pub fn center_in_rectangle(outer: &Rectangle, inner: &Rectangle) -> Coordinates {
	let center_x = outer.width / 2;
	let center_y = outer.height / 2;
	Coordinates {
		x: center_x - (inner.width / 2),
		y: center_y - (inner.height / 2),
	}
}

pub struct Context<'a> {
	pub connection: &'a xcb::Connection,
	pub window: Window<'a>,
	pub screen: &'a xcb::Screen<'a>,
}

fn width_set(rect: &Rectangle, width: i16) -> Rectangle {
	let height = (rect.height as f64 * width as f64) / (rect.width as f64);
	Rectangle {
		width,
		height: height.floor() as i16,
	}
}

fn height_set(rect: &Rectangle, height: i16) -> Rectangle {
	let width = (height as f64 * rect.width as f64) / (rect.height as f64);
	Rectangle {
		height,
		width: width.floor() as i16,
	}
}

pub fn resize_dimensions(outer: &Rectangle, inner: &Rectangle) -> Rectangle {
	if outer.width >= inner.width && outer.height >= inner.height {
		inner.clone()
	} else if outer.width < inner.width && outer.height < inner.height {
		if inner.width > inner.height {
			width_set(inner, outer.width)
		} else {
			height_set(inner, outer.height)
		}
	} else if outer.width < inner.width {
		width_set(inner, outer.width)
	} else {
		height_set(inner, outer.height)
	}
}

pub fn needs_resizing(outer: &Rectangle, inner: &Rectangle) -> bool {
	resize_dimensions(outer, inner) != *inner
}

fn image_extensions<'a>() -> HashSet<&'a str> {
	vec!["jpg", "jpeg", "bmp", "webp", "png"]
		.into_iter()
		.collect()
}

fn is_image(image_extensions: &HashSet<&str>, path: &Path) -> bool {
	path.extension()
		.and_then(OsStr::to_str)
		.map(|extension| image_extensions.contains(&extension))
		.unwrap_or(false)
}

pub fn get_image_list(path: &Path) -> Result<Vec<PathBuf>, Box<dyn Error>> {
	let image_directory = if path.is_dir() {
		path
	} else {
		path.parent().unwrap()
	};
	info!(
		"Trying to get image list from directory: {}",
		image_directory.display()
	);
	let extensions = image_extensions();

	let mut vec: Vec<PathBuf> = fs::read_dir(image_directory)?
		.filter(|file_result| file_result.is_ok())
		.map(|entry| entry.unwrap().path())
		.filter(|path| is_image(&extensions, &path))
		.collect();

	vec.sort();

	Ok(vec)
}

pub fn thumbnail_image_list(path: &Path) -> Result<Vec<PathBuf>, Box<dyn Error>> {
	if path.is_dir() {
		match get_image_list(path) {
			Err(e) => {
				error!(
					"Failed to get image list in thumbnail mode with error: {}",
					e
				);
				Err(e)
			}
			list => list,
		}
	} else {
		Ok(vec![path.to_path_buf()])
	}
}
