use crate::constants;
use crate::utils::RustyImageType;
use crate::window::Window;
use std::ffi::CString;

pub fn notify_window_close(connection: &xcb::Connection, window: &Window) -> Option<xcb::Atom> {
	let protocol_reply = xcb::intern_atom(connection, true, "WM_PROTOCOLS")
		.get_reply()
		.ok()?;
	let close_window_reply = xcb::intern_atom(connection, false, "WM_DELETE_WINDOW")
		.get_reply()
		.ok()?;
	xcb::change_property(
		connection,
		xcb::PROP_MODE_REPLACE as u8,
		window.xcb_window(),
		protocol_reply.atom(),
		xcb::ATOM_ATOM,
		constants::PROPERTY_ATOM_FORMAT,
		&[close_window_reply.atom()],
	);
	Some(close_window_reply.atom())
}

pub fn change_window_name(connection: &xcb::Connection, window: &Window, window_name: &CString) {
	xcb::change_property(
		connection,
		xcb::PROP_MODE_REPLACE as u8,
		window.xcb_window(),
		xcb::ATOM_WM_NAME,
		xcb::ATOM_STRING,
		constants::PROPERTY_STRING_FORMAT,
		window_name.to_bytes_with_nul(),
	);
}

pub fn window_size(image: &RustyImageType, screen: &xcb::Screen) -> (u16, u16) {
	if image.width() as u16 > screen.width_in_pixels()
		|| image.height() as u16 > screen.height_in_pixels()
	{
		(screen.width_in_pixels(), screen.height_in_pixels())
	} else {
		(image.width() as u16, image.height() as u16)
	}
}
