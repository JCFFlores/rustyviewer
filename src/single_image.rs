use std::ops::Drop;

use crate::utils;
use crate::window::Window;

pub struct SingleImage<'a> {
	connection: &'a xcb::Connection,
	pixmap: xcb::Pixmap,
	width: u16,
	height: u16,
	context: xcb::Gcontext,
}

impl<'a> SingleImage<'a> {
	pub fn new(
		base_image: &utils::RustyImageType,
		connection: &'a xcb::Connection,
		depth: u8,
		window: &Window,
	) -> SingleImage<'a> {
		let pixmap = connection.generate_id();
		let width = base_image.width() as u16;
		let height = base_image.height() as u16;
		xcb::create_pixmap(
			&connection,
			depth,
			pixmap,
			window.xcb_window(),
			width,
			height,
		);
		let context = connection.generate_id();
		xcb::create_gc(&connection, context, pixmap, &[]);
		xcb::put_image(
			&connection,
			xcb::IMAGE_FORMAT_Z_PIXMAP as u8,
			pixmap,
			context,
			width,
			height,
			0,
			0,
			0,
			depth,
			base_image.as_raw().as_slice(),
		);
		connection.flush();
		SingleImage {
			connection,
			pixmap,
			width,
			height,
			context,
		}
	}

	pub fn draw_image(&self, window: &Window) {
		let utils::Coordinates { x, y } =
			utils::center_in_rectangle(&window.as_rectangle(), &self.as_rectangle());
		xcb::copy_area(
			&self.connection,
			self.pixmap,
			window.xcb_window(),
			self.context,
			0,
			0,
			x,
			y,
			self.width,
			self.height,
		);
		self.connection.flush();
	}

	pub fn as_rectangle(&self) -> utils::Rectangle {
		utils::Rectangle {
			width: self.width as i16,
			height: self.height as i16,
		}
	}
}

impl<'a> Drop for SingleImage<'a> {
	fn drop(&mut self) {
		xcb::free_gc(&self.connection, self.context);
		xcb::free_pixmap(&self.connection, self.pixmap);
		self.connection.flush();
	}
}
