use crate::utils;
use crate::window::Window;
use std::ops::Drop;

pub struct Thumbnail<'a> {
	connection: &'a xcb::Connection,
	pixmap: xcb::Pixmap,
	width: u16,
	height: u16,
	image_context: xcb::Gcontext,
}

impl<'a> Thumbnail<'a> {
	pub fn new(
		base_image: &utils::RustyImageType,
		connection: &'a xcb::Connection,
		depth: u8,
		window: &Window,
	) -> Thumbnail<'a> {
		let pixmap = connection.generate_id();
		let width = base_image.width() as u16;
		let height = base_image.height() as u16;
		xcb::create_pixmap(
			&connection,
			depth,
			pixmap,
			window.xcb_window(),
			width,
			height,
		);
		let image_context = connection.generate_id();
		xcb::create_gc(&connection, image_context, pixmap, &[]);
		xcb::put_image(
			&connection,
			xcb::IMAGE_FORMAT_Z_PIXMAP as u8,
			pixmap,
			image_context,
			width,
			height,
			0,
			0,
			0,
			depth,
			base_image.as_raw().as_slice(),
		);
		connection.flush();
		Thumbnail {
			connection,
			pixmap,
			width,
			height,
			image_context,
		}
	}

	pub fn draw(
		&self,
		window: &Window,
		thumbnail_rect: &utils::Rectangle,
		thumbnail_coordinates: &utils::Coordinates,
		rectangle_context: xcb::Gcontext,
		selected: bool,
	) {
	    let image_rectangle = self.as_rectangle();
		let utils::Coordinates { x, y } =
			utils::center_in_rectangle(thumbnail_rect, &image_rectangle);
		let x_coord = x + thumbnail_coordinates.x;
		let y_coord = y + thumbnail_coordinates.y;
		xcb::copy_area(
			self.connection,
			self.pixmap,
			window.xcb_window(),
			self.image_context,
			0,
			0,
			x_coord,
			y_coord,
			self.width,
			self.height,
		);
	    if selected {
			let rect = xcb::Rectangle::new(
				x_coord,
				y_coord,
				image_rectangle.width as u16,
				image_rectangle.height as u16,
			);
			xcb::poly_rectangle(
				self.connection,
				window.xcb_window(),
				rectangle_context.clone(),
				&[rect],
			);
		}
	}

	pub fn as_rectangle(&self) -> utils::Rectangle {
		utils::Rectangle {
			width: self.width as i16,
			height: self.height as i16,
		}
	}
}

impl<'a> Drop for Thumbnail<'a> {
	fn drop(&mut self) {
		xcb::free_gc(&self.connection, self.image_context);
		xcb::free_pixmap(&self.connection, self.pixmap);
		self.connection.flush();
	}
}
